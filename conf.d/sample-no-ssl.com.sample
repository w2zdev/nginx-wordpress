server {
	## Ports to listen on
	listen 80;
	listen [::]:80;

	server_name www.sample-no-ssl.com;

	root /var/www/sample-no-ssl.com/public;

	index index.php index.html;

	## Overrides logs defined in nginx.conf, allows per site logs.
	error_log /var/www/sample-no-ssl.com/logs/error.log crit;
	access_log /var/www/sample-no-ssl.com/logs/access.log;

	include global/server/defaults.conf;
	include global/server/static-files.conf;

	location / {
		try_files $uri $uri/ /index.php?$args;
	}

	location ~ \.php$ {
		try_files $uri =404;
		include global/fastcgi-params.conf;

		## Use the php pool defined in the upstream variable.
		## See global/php-pool.conf for definition.
		fastcgi_pass   $upstream;
	}

	## Rewrite robots.txt
	rewrite ^/robots.txt$ /index.php last;
}

## Redirect non-www to www
server {
	listen 80;
	listen [::]:80;
	server_name sample-no-ssl.com;

	return 301 $scheme://www.sample-no-ssl.com$request_uri;
}